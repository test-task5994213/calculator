package com.example.calculator.model.dto;

import com.example.calculator.model.enumeration.CalculationType;
import lombok.Builder;

import java.util.List;

@Builder
public record InputServiceResponseDto(
        CalculationType calculationType,
        List<Number> numbers) {
}
