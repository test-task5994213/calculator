package com.example.calculator.model.dto;

import lombok.Builder;

@Builder
public record OutputServiceRequestDto(
        Number result,
        String filename) {
}
