package com.example.calculator.model.dto;

import lombok.Builder;

@Builder
public record InputServiceRequestDto(
        String filename) {
}
