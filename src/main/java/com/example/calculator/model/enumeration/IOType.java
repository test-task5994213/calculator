package com.example.calculator.model.enumeration;

public enum IOType {

    CONSOLE,
    FILE
}
