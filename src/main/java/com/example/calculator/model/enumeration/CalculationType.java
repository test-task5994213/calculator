package com.example.calculator.model.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public enum CalculationType {

    ADDITION("add"),
    MULTIPLICATION("mul"),
    MULTIPLICATION_ADDITION("mul-add"),
    SQUARE_ROOT("sqrt");

    private final String command;
}
