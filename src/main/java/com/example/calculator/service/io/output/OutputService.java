package com.example.calculator.service.io.output;

import com.example.calculator.model.dto.OutputServiceRequestDto;
import com.example.calculator.service.io.IOService;

public interface OutputService extends IOService {

    static String getResult(Number result) {
        return String.format("Ответ: %s", result);
    }

    void write(OutputServiceRequestDto request);
}
