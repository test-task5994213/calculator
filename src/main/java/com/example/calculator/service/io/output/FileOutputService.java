package com.example.calculator.service.io.output;

import com.example.calculator.model.dto.OutputServiceRequestDto;
import com.example.calculator.model.enumeration.IOType;
import org.springframework.stereotype.Service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Service
public class FileOutputService implements OutputService {

    @Override
    public IOType getIOType() {
        return IOType.FILE;
    }

    @Override
    public void write(OutputServiceRequestDto request) {
        if (request.filename() == null) {
            throw new RuntimeException("Filename is required.");
        }
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(request.filename()))) {
            writer.write(OutputService.getResult(request.result()));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
