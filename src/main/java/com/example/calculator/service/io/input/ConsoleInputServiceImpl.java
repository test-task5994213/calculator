package com.example.calculator.service.io.input;

import com.example.calculator.model.dto.InputServiceRequestDto;
import com.example.calculator.model.dto.InputServiceResponseDto;
import com.example.calculator.model.enumeration.IOType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Scanner;

@RequiredArgsConstructor
@Service
public class ConsoleInputServiceImpl implements InputService {

    private final Scanner scanner;

    @Override
    public IOType getIOType() {
        return IOType.CONSOLE;
    }

    @Override
    public InputServiceResponseDto read(InputServiceRequestDto request) {
        return InputService.build(scanner.nextLine());
    }
}
