package com.example.calculator.service.io.input;

import com.example.calculator.model.dto.InputServiceRequestDto;
import com.example.calculator.model.dto.InputServiceResponseDto;
import com.example.calculator.model.enumeration.IOType;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@Service
public class FileInputServiceImpl implements InputService {

    @Override
    public IOType getIOType() {
        return IOType.FILE;
    }

    @Override
    public InputServiceResponseDto read(InputServiceRequestDto request) {
        if (request.filename() == null) {
            throw new RuntimeException("Filename is required.");
        }
        try (BufferedReader reader = new BufferedReader(new FileReader(request.filename()))) {
            return InputService.build(reader.readLine());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
