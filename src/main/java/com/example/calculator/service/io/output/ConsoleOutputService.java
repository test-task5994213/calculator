package com.example.calculator.service.io.output;

import com.example.calculator.model.dto.OutputServiceRequestDto;
import com.example.calculator.model.enumeration.IOType;
import org.springframework.stereotype.Service;

@Service
public class ConsoleOutputService implements OutputService {

    @Override
    public IOType getIOType() {
        return IOType.CONSOLE;
    }

    @Override
    public void write(OutputServiceRequestDto request) {
        System.out.println(OutputService.getResult(request.result()));
    }
}
