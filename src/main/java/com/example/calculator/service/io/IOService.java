package com.example.calculator.service.io;

import com.example.calculator.model.enumeration.IOType;

public interface IOService {

    IOType getIOType();
}
