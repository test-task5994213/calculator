package com.example.calculator.service.io.input;

import com.example.calculator.model.dto.InputServiceRequestDto;
import com.example.calculator.model.dto.InputServiceResponseDto;
import com.example.calculator.model.enumeration.CalculationType;
import com.example.calculator.service.io.IOService;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.function.Function;
import java.util.regex.Pattern;

public interface InputService extends IOService {

    int INPUT_REQUIRED_ARGS_COUNT = 2;
    Pattern SPACE_CHARACTER_PATTERN = Pattern.compile(" ");
    List<String> COMMANDS = Arrays.stream(CalculationType.values())
            .map(CalculationType::getCommand)
            .toList();

    static InputServiceResponseDto build(String input) {
        String[] args = SPACE_CHARACTER_PATTERN.split(input);
        if (args.length < INPUT_REQUIRED_ARGS_COUNT) {
            throw new RuntimeException(String.format("Input arguments count must be at least %s.", INPUT_REQUIRED_ARGS_COUNT));
        }
        try {
            CalculationType calculationType = Arrays.stream(CalculationType.values())
                    .filter(type -> type.getCommand().equals(args[0]))
                    .findAny()
                    .orElseThrow();
            List<Number> numbers = Arrays.stream(args, 1, args.length)
                    .map((Function<String, Number>) Double::parseDouble)
                    .toList();
            return InputServiceResponseDto.builder()
                    .calculationType(calculationType)
                    .numbers(numbers)
                    .build();
        } catch (NoSuchElementException e) {
            throw new RuntimeException(String.format("Calculation command must be %s.", COMMANDS), e);
        } catch (NumberFormatException e) {
            throw new RuntimeException("Numbers format exception.", e);
        }
    }

    InputServiceResponseDto read(InputServiceRequestDto request);
}
