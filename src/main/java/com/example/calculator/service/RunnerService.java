package com.example.calculator.service;

public interface RunnerService {

    void run(String[] args);
}
