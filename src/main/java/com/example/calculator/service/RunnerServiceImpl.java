package com.example.calculator.service;

import com.example.calculator.model.dto.InputServiceRequestDto;
import com.example.calculator.model.dto.InputServiceResponseDto;
import com.example.calculator.model.dto.OutputServiceRequestDto;
import com.example.calculator.model.enumeration.IOType;
import com.example.calculator.service.calculation.CalculationService;
import com.example.calculator.service.io.input.InputService;
import com.example.calculator.service.io.output.OutputService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;

@RequiredArgsConstructor
@Service
public class RunnerServiceImpl implements RunnerService {

    private static final int REQUIRED_ARGS_COUNT = 2;
    private static final String CONSOLE_CHARACTER_ARG = "-";

    private final Collection<InputService> inputServices;
    private final Collection<OutputService> outputServices;
    private final Collection<CalculationService> calculationServices;

    @Override
    public void run(String[] args) {
        if (args.length < REQUIRED_ARGS_COUNT) {
            throw new RuntimeException("Input output args are required.");
        }
        InputServiceResponseDto response = readLine(args[0]);
        Number result = calculateResult(response);
        writeResult(args[1], result);
    }

    private InputServiceResponseDto readLine(String inputArg) {
        if (inputArg.equals(CONSOLE_CHARACTER_ARG)) {
            return getInputService(IOType.CONSOLE)
                    .read(null);
        } else {
            return getInputService(IOType.FILE)
                    .read(InputServiceRequestDto.builder()
                            .filename(inputArg)
                            .build());
        }
    }

    private InputService getInputService(IOType ioType) {
        return inputServices.stream()
                .filter(service -> service.getIOType() == ioType)
                .findAny()
                .orElseThrow();
    }

    private Number calculateResult(InputServiceResponseDto response) {
        return calculationServices.stream()
                .filter(calculationService -> calculationService.getCalculationType() == response.calculationType())
                .findAny()
                .orElseThrow()
                .calculate(response.numbers());
    }

    private void writeResult(String outputArg, Number result) {
        if (outputArg.equals(CONSOLE_CHARACTER_ARG)) {
            getOutputService(IOType.CONSOLE)
                    .write(OutputServiceRequestDto.builder()
                            .result(result)
                            .build());
        } else {
            getOutputService(IOType.FILE)
                    .write(OutputServiceRequestDto.builder()
                            .result(result)
                            .filename(outputArg)
                            .build());
        }
    }

    private OutputService getOutputService(IOType ioType) {
        return outputServices.stream()
                .filter(service -> service.getIOType() == ioType)
                .findAny()
                .orElseThrow();
    }
}
