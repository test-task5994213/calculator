package com.example.calculator.service.calculation;

import com.example.calculator.model.enumeration.CalculationType;

import java.util.List;

public interface CalculationService {

    CalculationType getCalculationType();

    Number calculate(List<Number> numbers);
}
