package com.example.calculator.service.calculation;

import com.example.calculator.model.enumeration.CalculationType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditionServiceImpl implements CalculationService {

    @Override
    public CalculationType getCalculationType() {
        return CalculationType.ADDITION;
    }

    @Override
    public Number calculate(List<Number> numbers) {
        return numbers.stream()
                .mapToDouble(Number::doubleValue)
                .sum();
    }
}
