package com.example.calculator.service.calculation;

import com.example.calculator.model.enumeration.CalculationType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SquareRootServiceImpl implements CalculationService {

    private static final int NUMBERS_COUNT = 1;

    @Override
    public CalculationType getCalculationType() {
        return CalculationType.SQUARE_ROOT;
    }

    @Override
    public Number calculate(List<Number> numbers) {
        if (numbers.size() != NUMBERS_COUNT) {
            throw new RuntimeException(String.format("Numbers count must be %s.", NUMBERS_COUNT));
        }
        return Math.sqrt(numbers.get(0).doubleValue());
    }
}
