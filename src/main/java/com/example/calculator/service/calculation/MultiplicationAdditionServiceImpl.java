package com.example.calculator.service.calculation;

import com.example.calculator.model.enumeration.CalculationType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MultiplicationAdditionServiceImpl implements CalculationService {

    private static final int NUMBERS_COUNT = 3;

    @Override
    public CalculationType getCalculationType() {
        return CalculationType.MULTIPLICATION_ADDITION;
    }

    @Override
    public Number calculate(List<Number> numbers) {
        if (numbers.size() != NUMBERS_COUNT) {
            throw new RuntimeException(String.format("Numbers count must be %s.", NUMBERS_COUNT));
        }
        return numbers.get(0).doubleValue() * numbers.get(1).doubleValue() + numbers.get(2).doubleValue();
    }
}
