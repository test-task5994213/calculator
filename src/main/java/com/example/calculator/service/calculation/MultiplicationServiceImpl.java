package com.example.calculator.service.calculation;

import com.example.calculator.model.enumeration.CalculationType;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MultiplicationServiceImpl implements CalculationService {

    @Override
    public CalculationType getCalculationType() {
        return CalculationType.MULTIPLICATION;
    }

    @Override
    public Number calculate(List<Number> numbers) {
        return numbers.stream()
                .reduce(1.0, (number1, number2) -> number1.doubleValue() * number2.doubleValue());
    }
}
