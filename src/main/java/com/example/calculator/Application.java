package com.example.calculator;

import com.example.calculator.config.ApplicationConfig;
import com.example.calculator.service.RunnerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        RunnerService runner = context.getBean(RunnerService.class);
        runner.run(args);
    }
}
